# Infusion #

Projekt strony zakodowany całkowicie samodzielnie na podstawie PSD, bez wykorzystania jakichkolwiek pluginów czy gotowych rozwiązań. 

### Technologie ###

* HTML5
* CSS3/SASS
* GULP
* JavaScript/jQuery



Głównym celem miało być stworzenie trzech różnych sliderów na podstawie danych z pliku JSON.



## [Zobacz stronę](https://mdobski.bitbucket.io) ##