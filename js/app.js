$(function () {
    
    //HERO SLIDER variables
    
    var heroSlider = $('#hero-slider').find('.hero-deck');
    var heroNav = $('#hero-slider').find('.hero-nav');
    var heroLeft = $('.hero-left');
    var heroRight = $('.hero-right');
    var heros;
    var counterHeros = 1;

    //IMAGE SLIDER variables
    
    var slider = $('#image-slider');
    var slides;
    var counter = 1;
    
    //QUOTE SLIDER variables
    
    var quoteSlider = $('#quote-slider').find('.quote-content');
    var quoteNav = $('#quote-slider').find('.quote-nav');
    var quotes;
    var counterQuotes = 1;
    
    //retrieve data from JSON file

    $.getJSON('sliderFeed.json', function (data) {
        
        var heroLength = data.heroSlider.length;
        var colDivide = 12 / heroLength;
        var colVal = Math.floor(colDivide);
        
        $.each(data.heroSlider, function() {
            //inject HERO SLIDER data to new elements in HTML
            heroSlider.append('<div class="hero-card col-xs-12 col-sm-6 col-md-'+colVal+'"><div><img src="'+this['thumbnail']['image']+'"><img src="'+this['thumbnail']['userImage']+'"></div><h3>'+this['thumbnail']['title']+'</h3><p>'+this['thumbnail']['text']+'</p></div>');
            
            heroLeft.after('<div class="dot"></div>');
                        
        })


        $.each(data.imageSlider, function () {
            //inject IMAGE SLIDER data to new elements in HTML
            slider.append('<div><img src="' + this['image'] + '"></div>');
            slides = slider.find('div:not([class*="-arw"])');
            
        })
        
        $.each(data.textSlider, function() {
            //inject QUOTE SLIDER data to new elements in HTML
            quoteSlider.append('<div><h3>"'+this['text']+'"</h3><p>'+this['releaseDate']+'</p><span>'+this['author']+'</span></div>');
            
            quoteNav.append('<div class="quote-dot"></div>');
            
        })
        
        var click = 'click';
        var swipeRight = 'swiperight';
        var swipeLeft = 'swipeleft';
        
        //HERO SLIDER
        
        heroSlider.find('.hero-card').eq(0).addClass('selected');
        var heroDots = $('.dot');
        heroDots.eq(0).addClass('dot-selected');
        
        heroLeft.on('click', function() {
            $('.selected').removeClass('selected').prev('.hero-card').addClass('selected');
            $('.dot-selected').removeClass('dot-selected').prev('.dot').addClass('dot-selected');
            counterHeros--;
            
            if (counterHeros < 1) {
                heroSlider.find('.hero-card').last().addClass('selected');
                heroDots.last().addClass('dot-selected');
                counterHeros = heroDots.length;
            }
            
        })
        
        heroRight.on('click', function() {
            $('.selected').removeClass('selected').next('.hero-card').addClass('selected');
            $('.dot-selected').removeClass('dot-selected').next('.dot').addClass('dot-selected');
            counterHeros++;
            
            if (counterHeros > heroDots.length) {
                heroSlider.find('.hero-card').eq(0).addClass('selected');
                heroDots.eq(0).addClass('dot-selected');
                counterHeros = 1;
            }
        })
        
        //IMAGE SLIDER
        
        console.log(slides);
        slides.eq(0).addClass('visible');

        var leftArw = $('.left-arw');
        var rightArw = $('.right-arw');
        function imageRight(x,y) {
            x.on(y, function () {
                $('#image-slider').find('.visible').removeClass('visible').next('div:not([class*="-arw"])').addClass('visible');
                counter++;

                if (counter > slides.length) {
                    slides.eq(0).addClass('visible');
                    counter = 1;
                }
            
            })
        }
        
        var visib = $('#image-slider').find('.visible');
        imageRight(rightArw, click);
        imageRight(visib, swipeRight); //swipe fail attempt
        
        
        leftArw.on('click', function() {
            $('#image-slider').find('.visible').removeClass('visible').prev('div:not([class*="-arw"])').addClass('visible');
            counter--;
            
            if (counter < 1) {
                slides.last().addClass('visible');
                counter = slides.length;
            }
        })
        
        //QUOTE SLIDER
        
        quotes = quoteSlider.find('div');
        quotes.eq(0).addClass('visible');
        var quoteDots = $('.quote-dot');
        quoteDots.eq(0).addClass('quote-dot-selected');
        
        var leftQuote = $('.quote-left');
        var rightQuote = $('.quote-right');
        
        rightQuote.on('click', function() {
            $('#quote-slider').find('.visible').removeClass('visible').next().addClass('visible');
            $('.quote-dot-selected').removeClass('quote-dot-selected').next().addClass('quote-dot-selected');
            counterQuotes++;
            
            if (counterQuotes > quotes.length) {
                quotes.eq(0).addClass('visible');
                quoteDots.eq(0).addClass('quote-dot-selected');
                counterQuotes = 1;
            }
        })
        
        leftQuote.on('click', function() {
            $('#quote-slider').find('.visible').removeClass('visible').prev().addClass('visible');
            $('.quote-dot-selected').removeClass('quote-dot-selected').prev().addClass('quote-dot-selected');
            counterQuotes--;
            
            if (counterQuotes < 1) {
                quotes.last().addClass('visible');
                quoteDots.last().addClass('quote-dot-selected');
                counterQuotes = quotes.length;
            }
        })
        
//        var quotesBoxH = 0;
//        for (var i = 0; i < quotes.length; i++) {
//            var value = quotes[i].offsetHeight;
//            
//            if (value > quotesBoxH) {
//                quotesBoxH = value;
//            }
//            console.log(value);
//        }
//        console.log(quotesBoxH,quotes);
//        
//        document.querySelector('.quote-content').style.minHeight = quotesBoxH+'px';
        
    })
    
    
    
    


})















